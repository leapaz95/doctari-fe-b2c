# Doctari base package

## Environment setup

### Create based on .env.template, in the project directory:

- Create production env: `cp .env.template .env.production`
- Create develop env: `cp .env.template .env.development`

Change the value that corresponds the environment.

## Installation

In the project directory:

- npm i to install dependencies.
- npm start to run the app in the development mode.
