import axiosInstance from './axiosConfig';

// * * * Login / Sign Up * * *
const getCountries = () => {
  const url = `country/`;
  const response = axiosInstance({
    method: 'GET',
    url,
  });

  return response;
};

const getSpecilities = () => {
  const url = `specility/`;
  const response = axiosInstance({
    method: 'GET',
    url,
  });

  return response;
};

const login = (credentials) => {
  const url = `login/`;
  const response = axiosInstance({
    method: 'POST',
    url,
    data: credentials,
  });

  return response;
};

const signup = (type, body) => {
  const url = `${type}/account/signup/`;
  const response = axiosInstance({
    method: 'POST',
    url,
    data: body,
  });

  return response;
};

export { login, signup, getCountries, getSpecilities };
