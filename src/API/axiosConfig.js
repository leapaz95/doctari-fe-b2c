import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  timeout: 10000,
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
  },
});

axiosInstance.interceptors.request.use(async (config) => {
  const headerType = config.contentType
    ? 'multipart/form-data'
    : 'application/json';

  if (config.needToken) {
    config.headers = {
      'Content-Type': headerType,
      Authorization: `JWT ${config.needToken}`,
    };
  } else {
    config.headers = {
      'Content-Type': headerType,
    };
  }
  return config;
});

axiosInstance.interceptors.response.use(
  (response) => {
    return response;
  },
  async (error) => {
    // if (
    //   error.response.status === 401 &&
    //   error.response.data &&
    //   error.response.data.errors[0].code === 'token_not_valid' &&
    //   error.response.data.errors[0].code ===
    //     'Given token not valid for any token type'
    // ) {
    //   store.dispatch(refreshAccessToken());
    //   const originalRequest = error.config;
    //   return axiosInstance(originalRequest);
    // }
    throw error;
  }
);

export default axiosInstance;
