import React from 'react';
import { Switch, Route, Redirect, useHistory } from 'react-router-dom';

import Login from './screens/login';
import { MAINPAGE, LOGIN } from './utils/urls';

function App() {
  const history = useHistory();
  const templateTest = () => {
    return (
      <div className='App'>
        <header className='App-header'>
          <img className='App-logo' alt='logo' />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className='App-link'
            href='https://reactjs.org'
            target='_blank'
            rel='noopener noreferrer'
          >
            PRUEBA TEMPLATE
          </a>
          <button onClick={() => history.push(LOGIN)}>Ir al login</button>
        </header>
      </div>
    );
  };

  return (
    <Switch>
      <Route path={MAINPAGE} component={templateTest} exact />
      <Route path={LOGIN} component={Login} exact />
      <Redirect to={MAINPAGE} />
    </Switch>
  );
}

export default App;
