import React, { useState } from 'react';
import { connect } from 'react-redux';
import {
  Grid,
  Button,
  Paper,
  Box,
  FormControl,
  Input,
  InputLabel,
  Link,
} from '@material-ui/core';
import LinearDeterminate from '../../components/LinearLoader';
import Header from '../../components/Header';
import Notification from '../../components/Notification';
import { userActionsTypes } from '../../redux/user/actions';
import { useStyles } from './style';
import { LoginForm } from 'frontend_doctari_core';
import SignUpForm from './SignUpForm';
import ForgotPassword from './ForgotPassword';

function Login({ userState, onLogin, onSignup, getInitialData }) {
  const classes = useStyles();
  const [state, setState] = useState({
    email: null,
    password: null,
    showSignUp: false,
    showResetPassword: false,
  });
  const { email, password, showSignUp, showResetPassword } = state;
  const { user, countries, specialities } = userState;
  const handleLogin = () => {
    if (email && password) {
      const credentials = {
        email,
        password,
      };
      onLogin(credentials);
    }
  };

  const validateEmail = (email) => {
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  };

  return (
    <>
      <Header />
      {userState.error && (
        <Notification type='error' message={userState.error} />
      )}
      {userState.loading && <LinearDeterminate />}
      {/* <LoginForm onLogin={onLogin} /> */}
      {showSignUp ? (
        <SignUpForm
          classes={classes}
          goLogin={() => setState({ ...state, showSignUp: false })}
          getInitialData={getInitialData}
          countries={countries}
          specialities={specialities}
          validateEmail={validateEmail}
          onSignup={onSignup}
        />
      ) : showResetPassword ? (
        <ForgotPassword
          classes={classes}
          goLogin={() => setState({ ...state, showResetPassword: false })}
          validateEmail={validateEmail}
        />
      ) : (
        <Grid
          container
          alignItems='center'
          style={{ minHeight: '100vh', backgroundColor: '#f1f1f1' }}
          justify='center'
        >
          <Grid item xs={11} md={3}>
            <Paper elevation={3}>
              <Box p={'13%'} display='flex' flexDirection='column'>
                <Box className={classes.H5}>Ingresar</Box>
                <FormControl
                  className={classes.formControl}
                  error={email === ''}
                >
                  <InputLabel htmlFor='my-input'>Email *</InputLabel>
                  <Input
                    id='my-input'
                    aria-describedby='my-helper-text'
                    onChange={(e) =>
                      setState({ ...state, email: e.target.value })
                    }
                    value={email}
                    required
                  />
                </FormControl>
                <FormControl
                  className={classes.formControl}
                  error={password === ''}
                >
                  <InputLabel htmlFor='my-input'>Contraseña *</InputLabel>
                  <Input
                    id='my-input'
                    aria-describedby='my-helper-text'
                    onChange={(e) =>
                      setState({ ...state, password: e.target.value })
                    }
                    value={password}
                    type='password'
                    color='primary'
                  />
                </FormControl>
                <Button
                  variant='contained'
                  color='primary'
                  onClick={handleLogin}
                  disabled={!validateEmail(email) || !password}
                >
                  INGRESAR
                </Button>
                <Button
                  variant='outlined'
                  color='primary'
                  className={classes.register}
                  onClick={() => setState({ ...state, showSignUp: true })}
                >
                  REGÍSTRATE
                </Button>
              </Box>
            </Paper>
            <Box className={classes.helpText}>
              <Link
                style={{ cursor: 'pointer' }}
                onClick={() => setState({ ...state, showResetPassword: true })}
              >
                ¿Olvidaste tú contraseña?
              </Link>
            </Box>
          </Grid>
        </Grid>
      )}
    </>
  );
}

const mapStateToProps = ({ user }) => {
  return {
    userState: user,
  };
};

const mapDispatchToProps = (dispatch) => ({
  onLogin: (credentials) =>
    dispatch({ type: userActionsTypes.USER_LOGIN, payload: credentials }),
  getInitialData: () => dispatch({ type: userActionsTypes.GET_SIGNUP_DATA }),
  onSignup: (type, body) =>
    dispatch({ type: userActionsTypes.USER_SIGNUP, payload: { type, body } }),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
