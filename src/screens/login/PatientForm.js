import React, { useState } from 'react';
import {
  Grid,
  Button,
  Box,
  FormControlLabel,
  RadioGroup,
  Radio,
  FormLabel,
  FormHelperText,
  Checkbox,
  FormControl,
  Input,
  InputLabel,
  Select,
  Link,
} from '@material-ui/core';
import { AddCircle } from '@material-ui/icons';
import PasswordInput from '../PasswordInput';
import DoctariModal from '../modal';

function ProfessionalForm({ onSignUp, classes, countries, validateEmail }) {
  const [state, setState] = useState({
    name: null,
    lastName: null,
    country: null,
    document: null,
    email: null,
    password: null,
    password2: null,
    termsAndConditions: false,
    modalOpen: false,
    institute: null,
    plan: null,
    city: null,
    gender: null,
  });

  const {
    name,
    lastName,
    country,
    document,
    email,
    password,
    password2,
    termsAndConditions,
    modalOpen,
    institute,
    plan,
    city,
    gender,
  } = state;

  const isValidPassword = (password) => {
    let passError = false;
    const strongRegex = new RegExp(
      '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})'
    );

    if (strongRegex.test(password)) {
      passError = true;
    }
    return passError;
  };

  const isEnable = () => {
    let isEnable = true;
    let obj = null;
    for (const prop in state) {
      obj = state[prop];
      if (prop !== 'modalOpen' && prop !== 'institute' && prop !== 'plan') {
        if (obj && (prop === 'password' || prop === 'password2')) {
          isEnable =
            isEnable && password2 === password && isValidPassword(password);
        } else if (obj && prop === 'email') {
          isEnable = isEnable && validateEmail(email);
        } else {
          isEnable = isEnable && obj;
        }
      }
    }
    return isEnable;
  };

  const selectInstitution = () => {
    return (
      <Box className={classes.modalBody}>
        <span className={classes.SPAN}>Seleccione su institución</span>
        <FormControl className={classes.formControl}>
          <InputLabel id='institute-native-simple'>Institución *</InputLabel>
          <Select
            inputProps={{
              name: 'institution',
              id: 'institute-native-simple',
            }}
            native
            value={institute}
            onChange={(e) => setState({ ...state, institute: e.target.value })}
          >
            <option aria-label='None' value='' />
          </Select>
        </FormControl>
        <span className={classes.SPAN}>Seleccione su plan de salud</span>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor='plan-native-simple'>Plan de salud *</InputLabel>
          <Select
            inputProps={{
              name: 'plan',
              id: 'plan-native-simple',
            }}
            native
            value={plan}
            onChange={(e) => setState({ ...state, plan: e.target.value })}
          >
            <option aria-label='None' value='' />
          </Select>
        </FormControl>
        <Grid
          item
          xs={12}
          md={12}
          alignContent='space-between'
          style={{ display: 'flex' }}
        >
          <Button
            fullWidth
            color='primary'
            onClick={() => setState({ ...state, modalOpen: false })}
          >
            Cancelar
          </Button>
          <Button fullWidth color='primary' disabled={!institute || !plan}>
            Agregar
          </Button>
        </Grid>
      </Box>
    );
  };

  return (
    <>
      <DoctariModal
        open={modalOpen}
        setOpen={(val) => setState({ ...state, modalOpen: val })}
        title={'Agregar institución'}
      >
        {selectInstitution()}
      </DoctariModal>
      <FormControl className={classes.formControl} error={name === ''}>
        <InputLabel htmlFor='my-input'>Nombre *</InputLabel>
        <Input
          id='my-input'
          aria-describedby='my-helper-text'
          onChange={(e) => setState({ ...state, name: e.target.value })}
          value={name}
          required
        />
      </FormControl>
      <FormControl className={classes.formControl} error={lastName === ''}>
        <InputLabel htmlFor='my-input'>Apellido *</InputLabel>
        <Input
          id='my-input'
          aria-describedby='my-helper-text'
          onChange={(e) => setState({ ...state, lastName: e.target.value })}
          value={lastName}
          required
        />
      </FormControl>
      <FormControl className={classes.formControl} error={country === ''}>
        <InputLabel id='demo-simple-select-label'>
          Seleccione un país *
        </InputLabel>
        <Select
          inputProps={{
            name: 'age',
            id: 'age-native-simple',
          }}
          native
          value={country}
          onChange={(e) => setState({ ...state, country: e.target.value })}
        >
          <option aria-label='None' value='' />
          {countries &&
            countries.map((country) => {
              return <option value={country.name}>{country.name}</option>;
            })}
        </Select>
      </FormControl>
      <FormControl className={classes.formControl} error={city === ''}>
        <InputLabel htmlFor='my-input'>Ciudad *</InputLabel>
        <Input
          id='my-input'
          aria-describedby='my-helper-text'
          onChange={(e) => setState({ ...state, city: e.target.value })}
          value={city}
          required
        />
      </FormControl>
      <FormControl className={classes.formControl} error={document === ''}>
        <InputLabel htmlFor='my-input'>Documento *</InputLabel>
        <Input
          id='my-input'
          aria-describedby='my-helper-text'
          onChange={(e) => setState({ ...state, document: e.target.value })}
          value={document}
          required
        />
      </FormControl>
      <FormControl
        className={classes.formControl}
        style={{ marginTop: '1rem', marginBottom: '0rem' }}
        error={gender === ''}
      >
        <FormLabel component='legend'>Género</FormLabel>
        <RadioGroup
          aria-label='gender'
          name='gender1'
          onChange={(e) => setState({ ...state, gender: e.target.value })}
          value={gender}
        >
          <FormControlLabel
            value='Femenino'
            control={<Radio />}
            label='Femenino'
          />
          <FormControlLabel
            value='Masculino'
            control={<Radio />}
            label='Masculino'
          />
          <FormControlLabel value='Otro' control={<Radio />} label='Otro' />
        </RadioGroup>
      </FormControl>
      <FormControl
        className={classes.formControl}
        error={email && !validateEmail(email)}
      >
        <InputLabel htmlFor='my-input'>Email *</InputLabel>
        <Input
          id='my-input'
          aria-describedby='my-helper-text'
          onChange={(e) => setState({ ...state, email: e.target.value })}
          value={email}
          required
        />
        {email && !validateEmail(email) && (
          <FormHelperText id='standard-adornment-password-text' error>
            Fromato inválido
          </FormHelperText>
        )}
      </FormControl>
      <PasswordInput
        classes={classes}
        changePassword={(newPass) => setState({ ...state, password: newPass })}
        password={password}
        isValidPassword={isValidPassword}
        labelText={'Contraseña *'}
      />
      <PasswordInput
        classes={classes}
        changePassword={(newPass) => setState({ ...state, password2: newPass })}
        password={password2}
        diffPassword={!password || password2 !== password}
        isSecondPassword
        labelText={'Confirmar contraseña *'}
      />
      {country && (
        <Grid item>
          <InputLabel className={classes.label}>Institución</InputLabel>
          <Button
            color='primary'
            startIcon={<AddCircle />}
            style={{ textTransform: 'none', paddingLeft: '0px' }}
            onClick={() => setState({ ...state, modalOpen: true })}
          >
            Agregar institución
          </Button>
        </Grid>
      )}
      <Grid item className={classes.formControl}>
        <FormControlLabel
          control={
            <Checkbox
              checked={termsAndConditions}
              onChange={(event) =>
                setState({ ...state, termsAndConditions: event.target.checked })
              }
              name='checkedB'
              color='primary'
            />
          }
          style={{ marginRight: '0px' }}
        />
        <span>Aceptar los</span>
        <Link href='#'> términos, condiciones y consentimiento informado</Link>
      </Grid>
      <Button
        variant='outlined'
        color='primary'
        className={classes.register}
        fullWidth
        disabled={!isEnable()}
      >
        REGÍSTRATE
      </Button>
    </>
  );
}

export default ProfessionalForm;
