import React, { useState } from 'react';
import {
  Grid,
  Button,
  Paper,
  Box,
  FormControl,
  Input,
  InputLabel,
  Link,
} from '@material-ui/core';

function ForgotPassword({ goLogin, classes, validateEmail }) {
  const [state, setState] = useState({
    email: null,
  });
  const { email } = state;

  function handleSendEmail() {
    // if (email && password) {
    //   const credentials = {
    //     email,
    //     password,
    //   };
    //   onLogin(credentials);
    // }
  }

  return (
    <Grid
      container
      alignItems='center'
      style={{ minHeight: '100vh', backgroundColor: '#f1f1f1' }}
      justify='center'
    >
      <Grid item xs={11} md={3}>
        <Paper elevation={3}>
          <Box p={'13%'} display='flex' flexDirection='column'>
            <Box className={classes.H5}>Recuperar contraseña</Box>
            <FormControl className={classes.formControl} error={email === ''}>
              <InputLabel htmlFor='my-input'>Email *</InputLabel>
              <Input
                id='my-input'
                aria-describedby='my-helper-text'
                onChange={(e) => setState({ ...state, email: e.target.value })}
                value={email}
                required
              />
            </FormControl>

            <Button
              variant='contained'
              color='primary'
              onClick={handleSendEmail}
              disabled={!validateEmail(email)}
            >
              ENVIAR EMAIL
            </Button>
          </Box>
        </Paper>
        <Box className={classes.helpText}>
          <Link style={{ cursor: 'pointer' }} onClick={goLogin}>
            Volver al inicio de sesión
          </Link>
        </Box>
      </Grid>
    </Grid>
  );
}

export default ForgotPassword;
