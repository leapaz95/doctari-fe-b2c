import { userActionsTypes } from './actions';

const INITIAL_STATE = {
  user: null,
  countries: [],
  specialities: [],
  error: null,
  loading: false,
};

const SESSION_INITIAL_STATE = {
  email: null,
  id: null,
  userType: null,
  accessToken: null,
};

const sessionReducer = (state = SESSION_INITIAL_STATE, actions) => {
  let new_state;
  switch (actions.type) {
    case userActionsTypes.SUCCESS_USER_LOGIN:
      new_state = {
        ...state,
        error: null,
        email: actions.payload.user.email,
        id: actions.payload.user.id,
        userType: actions.payload.user.type,
        accessToken: actions.payload.token,
      };
      break;

    case userActionsTypes.LOGOUT:
      new_state = SESSION_INITIAL_STATE;
      break;
    default:
      new_state = state;
  }
  return new_state;
};

const userReducer = (state = INITIAL_STATE, actions) => {
  let new_state;
  switch (actions.type) {
    case userActionsTypes.SUCCESS_USER_LOGIN:
      new_state = {
        ...state,
        error: null,
        user: actions.payload.user,
      };
      break;
    case userActionsTypes.SUCCESS_SIGNUP_DATA:
      new_state = {
        ...state,
        error: null,
        countries: actions.payload.countries,
        specialities: actions.payload.specialities,
      };
      break;
    case userActionsTypes.LOGOUT:
      new_state = INITIAL_STATE;
      break;
    case userActionsTypes.LOADING:
      new_state = {
        ...state,
        loading: actions.payload,
        error: actions.payload ? null : state.error,
      };
      break;
    case userActionsTypes.ERROR:
      new_state = {
        ...state,
        error: actions.payload,
      };
      break;
    default:
      new_state = state;
  }
  return new_state;
};

export { userReducer, sessionReducer };
