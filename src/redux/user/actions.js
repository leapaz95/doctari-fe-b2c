export const userActionsTypes = {
  USER_LOGIN: 'USER_LOGIN',
  GET_SIGNUP_DATA: 'GET_SIGNUP_DATA',
  USER_SIGNUP: 'USER_SIGNUP',
  SUCCESS_USER_LOGIN: 'SUCCESS_USER_LOGIN',
  SUCCESS_SIGNUP_DATA: 'SUCCESS_SIGNUP_DATA',
  SUCCESS_CREATE_PROFESSIONAL: 'SUCCESS_CREATE_PROFESSIONAL',
  SUCCESS_SIGNUP_PATIENT: 'SUCCESS_SIGNUP_PATIENT',
  LOGOUT: 'LOGOUT',
  ERROR: 'ERROR_LOGIN',
  LOADING: 'LOADING_LOGIN',
};

export const userActions = {
  successLogin: (user) => ({
    type: userActionsTypes.SUCCESS_USER_LOGIN,
    payload: user,
  }),
  successSignupPatient: (user) => ({
    type: userActionsTypes.SUCCESS_SIGNUP_PATIENT,
    payload: user,
  }),
  successCreateProfessional: () => ({
    type: userActionsTypes.SUCCESS_CREATE_PROFESSIONAL,
  }),
  successGetSignupData: (data) => ({
    type: userActionsTypes.SUCCESS_SIGNUP_DATA,
    payload: data,
  }),
  error: (message) => ({
    type: userActionsTypes.ERROR,
    payload: message,
  }),
  loading: (value) => ({
    type: userActionsTypes.LOADING,
    payload: value,
  }),
  logOut: () => ({
    type: userActionsTypes.LOGOUT,
  }),
};
