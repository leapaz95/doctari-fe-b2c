import { call, put, takeEvery, all } from 'redux-saga/effects';
import {
  login,
  getSpecilities,
  getCountries,
  signup,
} from '../../API/endpoints';
import { userActionsTypes, userActions } from './actions';

// Worker saga will be fired on USER_LOGIN actions
function* loginUser(action) {
  yield put(userActions.loading(true));
  try {
    const user = yield call(login, action.payload);
    yield put(userActions.successLogin(user.data));
  } catch (e) {
    yield put(userActions.error('Nombre de usuario o contraseña inválidos'));
  }
  yield put(userActions.loading(false));
}

function* createUser(action) {
  yield put(userActions.loading(true));
  try {
    const user = yield call(signup, action.payload.type, action.payload.body);
    if (action.payload.type === 'patient') {
      yield put(userActions.successSignupPatient(user.data));
    } else {
      yield put(userActions.successCreateProfessional());
    }
  } catch (e) {
    yield put(userActions.error('Error al crear el usuario'));
  }
  yield put(userActions.loading(false));
}

function* getInitialData() {
  yield put(userActions.loading(true));
  try {
    const [countries, specialities] = yield all([
      call(getCountries),
      call(getSpecilities),
    ]);

    const data = {
      countries: countries.data,
      specialities: specialities.data,
    };

    yield put(userActions.successGetSignupData(data));
  } catch (e) {
    yield put(userActions.error('Error al traer los datos'));
  }
  yield put(userActions.loading(false));
}

// Starts loginUser on each dispatched USER_LOGIN action
export default function* userSaga() {
  yield takeEvery(userActionsTypes.USER_LOGIN, loginUser);
  yield takeEvery(userActionsTypes.GET_SIGNUP_DATA, getInitialData);
  yield takeEvery(userActionsTypes.USER_SIGNUP, createUser);
}
