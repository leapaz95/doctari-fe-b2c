import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { userReducer, sessionReducer } from './user/reducer';

const persistConfig = {
  key: 'session',
  storage,
  whitelist: ['session'],
};

const rootReducer = combineReducers({
  user: userReducer,
  session: sessionReducer,
});

export default persistReducer(persistConfig, rootReducer);
