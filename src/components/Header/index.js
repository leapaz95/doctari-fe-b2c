import React from 'react';
import { makeStyles } from '@material-ui/styles';
import doctariLogo from '../../assets/doctari_logo.png';

const useStyles = makeStyles((theme) => ({
  header: {
    height: '6rem',
    backgroundColor: theme.palette.primary.main,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  img: {
    height: '3.5rem',
  },
}));

function LinearDeterminate() {
  const classes = useStyles();
  return (
    <header>
      <div className={classes.header}>
        <img alt='texto' src={doctariLogo} className={classes.img} />
      </div>
    </header>
  );
}

export default LinearDeterminate;
